package nkotb1.pkg2;

import java.sql.SQLException;
import java.util.Scanner;

public class Menu {

    public void menuChoice() throws SQLException {

        while (true) {
            Login login = new Login();
            Scanner input = new Scanner(System.in);
            User user = login.userInput(input);
            DbHandler dbhandler = new DbHandler();
            
            if (!dbhandler.query(user.getUsername(), user.getPassword())) {
                System.out.println("Invalid username or password.");
                break;
            }
            user.userData(dbhandler);
            
            System.out.println("A: Online bank.");
            System.out.println("B: ATM.");

            String userInput = input.next();

            if (userInput.equals("A")) {

                System.out.println("A: Show accounts.");
                System.out.println("B: Show loans.");
                System.out.println("C: Transaction history.");
                System.out.println("D: Transfer between personal accounts.");
                System.out.println("E: Transfer to other account.");
                String userInput1 = input.next();

                Account account = new Account();
                Transactions transactions = new Transactions();
                Transfer transfer = new Transfer();

                switch (userInput1) {
                    case "A":
                        account.accountsOverview();
                    case "B":
                        account.loansOverview();
                    case "C":
                        System.out.println("A: Show deposits.");
                        System.out.println("B: Show withdrawals.");
                        System.out.println("C: Show transfers.");

                        switch (userInput) {
                            case "A":
                                transactions.deposits();
                            case "B":
                                transactions.withdrawals();
                            case "C":
                                transactions.transfers();
                        }
                    case "D":
                        transfer.personalTransfer();

                    case "E":
                        transfer.otherTransfer(dbhandler, user);

                }

            } else if (userInput.equals("B")) {
                System.out.println("A: Deposit.");
                System.out.println("B: Withdraw.");
                System.out.println("C: Latest transactions.");
                ATM atm = new ATM();

                switch (userInput) {
                    case "A":
                        atm.depositCash();

                    case "B":
                        atm.withdrawCash();

                    case "C":
                        atm.latestTransactions();

                }
            } else {
                System.out.println("Invalid choice, please try again.");

            }
        }
    }

}
