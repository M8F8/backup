package nkotb1.pkg2;

import java.util.ArrayList;
import java.util.Scanner;
import java.sql.*;

public class User {

    private String username;
    private String password;
    private ArrayList<Integer> accounts;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void showDescription() {
        System.out.println("Welcome, " + username + ".");
    }
    
    public void setAccount(int ID) {
        this.accounts = accounts;
    }

    public ArrayList<Integer> getAccount() {
        return accounts;
    }
    //Arraylist som hämtar användarinfo från databas.
    public void userData(DbHandler db) throws SQLException {
        accounts = new ArrayList<Integer>(); 
        ResultSet r = db.executeQuery("SELECT * FROM customers WHERE Firstname = '" + getUsername() + "';");

        while (r.next()) {
            accounts.add(r.getInt("CustomerID"));
        }

    }
}
