package nkotb1.pkg2;

import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.ResultSet;

public class Account {

    Statement stmt = null;
    PreparedStatement pstmt = null;
    ResultSet rs = null;
    private String accountNumber;
    private int balance;
    private int savings;

    public void deposit(String username, String password) {

    }

    public void withdraw() {

    }

    public void accountsOverview() {

    }

    public void loansOverview() {
        try {
            DbHandler conObj = new DbHandler();
            conObj.dbConnection();
            stmt = conObj.dbConnection().prepareStatement("SELECT Loan FROM Accounts; ");
        } catch (SQLException exc) {
            System.out.println(exc.getMessage());
        }
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int personal) {
        this.balance = personal;
    }

    public int getSavings() {
        return savings;
    }

    public void setSavings(int savings) {
        this.savings = savings;
    }
}
